#!/usr/bin/env python3

import serial
import time

def send(message):
    with serial.Serial('/dev/ttyS0', 9600, timeout=1) as ser:
        try:
            ser.write( message+"\n".encode() )
            return
        except SerialException as e:
            print(e)

